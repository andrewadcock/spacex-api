import React from "react";
import { shallow } from "enzyme";
import ShipsPage from "./Ships";

describe("Ships.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ShipsPage />);
  });

  test("Check that page title shows", () => {
    expect(wrapper.find({ "data-testid": "page-title" }).text()).toEqual(
      "Ships"
    );
  });
});
