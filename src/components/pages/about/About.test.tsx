import React from "react";
import { shallow } from "enzyme";
import AboutPage from "./About";

describe("About.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<AboutPage />);
  });

  test("Check that page title shows", () => {
    expect(wrapper.find({ "data-testid": "page-title" }).text()).toEqual(
      "About"
    );
  });
});
