import React from "react";
import { shallow } from "enzyme";
import ShipPage from "./Ship";

describe("ShipListItem.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ShipPage />);
  });

  test("Check that page title shows", () => {
    expect(wrapper.find({ "data-testid": "page-title" }).text()).toEqual(
      "Ship"
    );
  });
});
