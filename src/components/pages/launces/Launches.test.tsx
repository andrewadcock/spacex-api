import React from "react";
import { shallow } from "enzyme";
import LaunchesPage from "./Launches";

describe("Launches.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<LaunchesPage />);
  });

  test("Check that page title shows", () => {
    expect(wrapper.find({ "data-testid": "page-title" }).text()).toEqual(
      "Launches"
    );
  });
});
