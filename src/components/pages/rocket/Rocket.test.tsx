import React from "react";
import { shallow } from "enzyme";
import RocketPage from "./Rocket";

describe("RocketListItem.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<RocketPage />);
  });

  test("Check that page title shows", () => {
    expect(wrapper.find({ "data-testid": "page-title" }).text()).toEqual(
      "Rocket"
    );
  });
});
