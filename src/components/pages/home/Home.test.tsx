import React from "react";
import { mount } from "enzyme";
import HomePage from "./Home";
import Launches from "../../components/launches/Launches";
import Ships from "../../components/ships/Ships";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import Rockets from "../../components/rockets/Rockets";
import { BrowserRouter } from "react-router-dom";

let store;
let mockStore = configureMockStore();

const storeData = {
  auth: {
    loginError: {
      active: true,
      message: "Test Error Message",
    },
  },
};

describe("Home.tsx", () => {
  let wrapper;
  store = mockStore(storeData);

  beforeEach(() => {
    wrapper = mount(
      <BrowserRouter>
        <Provider store={store}>
          <HomePage />
        </Provider>
      </BrowserRouter>
    );
  });

  test("renders Launches and Ships components", () => {
    expect(wrapper.containsMatchingElement(<Launches />)).toEqual(true);
    expect(wrapper.containsMatchingElement(<Ships />)).toEqual(true);
    expect(wrapper.containsMatchingElement(<Rockets />)).toEqual(true);
  });
});
