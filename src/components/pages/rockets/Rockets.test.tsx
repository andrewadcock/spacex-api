import React from "react";
import { shallow } from "enzyme";
import RocketsPage from "./Rockets";

describe("Rockets.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<RocketsPage />);
  });

  test("Check that page title shows", () => {
    expect(wrapper.find({ "data-testid": "page-title" }).text()).toEqual(
      "Rockets"
    );
  });
});
