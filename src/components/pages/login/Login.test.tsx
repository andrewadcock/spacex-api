import React from "react";
import { mount } from "enzyme";
import LoginPage from "./Login";
import Login from "../../components/login/Login";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";

let store;
let wrapper;
let mockStore = configureMockStore([]);

describe("Login.tsx", () => {
  const storeData = {
    auth: {
      loginError: {
        active: true,
        message: "Test Error Message",
      },
    },
  };

  beforeEach(() => {
    store = mockStore(storeData);
    wrapper = mount(
      <Provider store={store}>
        <LoginPage />
      </Provider>
    );
  });

  test("Check that page title shows", () => {
    expect(wrapper.find({ "data-testid": "page-title" }).text()).toEqual(
      "Login"
    );
  });

  test("renders login component", () => {
    expect(wrapper.containsMatchingElement(<Login />)).toEqual(true);
  });
});
