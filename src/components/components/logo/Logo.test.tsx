import React from "react";
import { shallow } from "enzyme";
import Logo from "./Logo";

describe("Logo.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Logo />);
  });

  test("Check that site motto shows", () => {
    expect(wrapper.find(".tag").text()).toEqual("An API for");
  });
  test("Check that site motto shows", () => {
    expect(wrapper.find(".logo-name").text()).toEqual("SPACE-X");
  });
  test("Check that site motto shows", () => {
    expect(wrapper.find(".motto").text()).toEqual("A DIGITAL EXPERIENCE");
  });
});
