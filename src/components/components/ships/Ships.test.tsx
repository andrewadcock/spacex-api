import React from "react";
import { mount } from "enzyme";
import Ships from "./Ships";

describe("Ships.tsx", () => {
  let wrapper;

  beforeEach(() => {
    // Add the table to prevent errors in testing (can't have tr inside a div)
    wrapper = mount(<Ships />);
  });

  test("Check the title", () => {
    expect(wrapper.find("h3")).toHaveLength(1);
    expect(wrapper.find("h3").text()).toEqual("Ships");
  });
});
