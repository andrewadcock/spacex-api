import { useEffect, useState } from "react";
import ShipListItem from "../shipListItem/ShipListItem";
import Anchor from "../../fields/anchor/Anchor";
import spaceXQuery from "../../../api/spacex";

const Ships = () => {
  const pathname = window.location.pathname;
  const queryOffset = parseInt(pathname.split("/")[2]);

  const [ships, setShips] = useState<any>();
  const [nav, setNav] = useState<any>({
    hasNextPage: false,
    hasPrevPage: false,
  });

  useEffect(() => {
    // Generate options array for spaceX API call
    const options = {
      endpoint: "ships",
      query: {
        active: {
          $eq: true,
        },
      },
      options: {
        limit: 10,
        sort: { name: "asc" },
        offset: queryOffset,
        select: {
          id: 1,
          image: 1,
          name: 1,
          year_built: 1,
        },
      },
    };

    // Call action creator to get a list of launches for the table below
    const getData = async () => {
      const data = await spaceXQuery(options);

      setShips(data.docs);
      setNav({
        hasNextPage: data.hasNextPage,
        hasPrevPage: data.hasPrevPage,
        prevPage: data.prevPage ?? 0,
        nextPage: data.nextPage ?? 0,
      });
    };

    getData().then();
  }, [queryOffset]);

  return (
    <div className="box-glow py-4 px-6 m-6">
      <h3 className="text-xl border-b-2 border-secondary text-secondary mb-2">
        Ships
      </h3>
      <p className="pb-6 uppercase text-sm">
        {ships ? ships.length : null} Ships, Alphabetically
      </p>
      {ships ? (
        <table className="table-fixed">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Year Built</th>
            </tr>
          </thead>
          <tbody>
            {ships.map((ship: any, index: number) => {
              const shipWithIndex = {
                ...ship,
                index,
              };
              return <ShipListItem ship={shipWithIndex} key={index} />;
            })}
          </tbody>
        </table>
      ) : (
        <p>Ships loading...</p>
      )}
      <div className="flex">
        <div className="flex-1">
          {nav.hasPrevPage ? (
            <Anchor href={`/ships/${nav.prevPage}`}>Previous</Anchor>
          ) : null}
        </div>

        <div className="flex-1 text-right">
          {nav.hasNextPage ? (
            <Anchor href={`/ships/${nav.nextPage}`}>Next</Anchor>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Ships;
