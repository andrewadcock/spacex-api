import React from "react";
import { mount } from "enzyme";
import Slider from "./Slider";

describe("LaunchListItem.tsx", () => {
  let wrapper;

  const slides = [
    "https://farm1.staticflickr.com/929/28787338307_3453a11a77_b.jpg",
    "https://farm4.staticflickr.com/3955/32915197674_eee74d81bb_b.jpg",
    "https://farm1.staticflickr.com/293/32312415025_6841e30bf1_b.jpg",
    "https://farm1.staticflickr.com/623/23660653516_5b6cb301d1_b.jpg",
    "https://farm6.staticflickr.com/5518/31579784413_d853331601_b.jpg",
    "https://farm1.staticflickr.com/745/32394687645_a9c54a34ef_b.jpg",
  ];

  beforeEach(() => {
    // Add the table to prevent errors in testing (can't have tr inside a div)
    wrapper = mount(<Slider slides={slides} />);
  });

  test("Check that slider icons are showing", () => {
    expect(wrapper.find("FaArrowAltCircleRight")).toHaveLength(1);
    expect(wrapper.find("FaArrowAltCircleLeft")).toHaveLength(1);
  });
});
