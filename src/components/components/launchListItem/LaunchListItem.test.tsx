import React from "react";
import { mount } from "enzyme";
import LaunchListItem from "./LaunchListItem";
import { BrowserRouter } from "react-router-dom";

describe("LaunchListItem.tsx", () => {
  let wrapper;

  const launch = {
    success: true,
    flight_number: 132,
    name: "Transporter-2",
    date_utc: "2021-06-30T19:31:00.000Z",
    id: "600f9b6d8f798e2a4d5f979f",
    index: 0,
    ships: ["60c8c7a45d4819007ea69871"],
  };

  beforeEach(() => {
    // Add the table to prevent errors in testing (can't have tr inside a div)
    wrapper = mount(
      <BrowserRouter>
        <table>
          <tbody>
            <LaunchListItem launch={launch} />
          </tbody>
        </table>
      </BrowserRouter>
    );
  });

  test("Check number of table columns", () => {
    expect(wrapper.find("td")).toHaveLength(3);
  });

  test("Check that launchListItem date renders", () => {
    expect(wrapper.find("td").at(1).text()).toEqual("5/30/21");
  });

  test("Check that year flight number renders", () => {
    expect(wrapper.find("td").at(2).text()).toEqual("132");
  });
});
