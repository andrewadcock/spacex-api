import React from "react";
import { shallow } from "enzyme";
import NavMain from "../navMain/NavMain";
import Anchor from "../../fields/anchor/Anchor";

describe("NavMain.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<NavMain />);
  });

  test("Check that there are four links", () => {
    expect(wrapper.find(Anchor)).toHaveLength(4);
  });
});
