import React from "react";
import { shallow } from "enzyme";
import Footer from "./Footer";
import Anchor from "../../fields/anchor/Anchor";

describe("Footer.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Footer />);
  });

  test("Check that the correct number of anchors shows", () => {
    expect(wrapper.find(Anchor)).toHaveLength(2);
  });
});
