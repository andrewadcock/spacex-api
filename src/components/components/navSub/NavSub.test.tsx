import React from "react";
import { shallow } from "enzyme";
import Anchor from "../../fields/anchor/Anchor";
import NavSub from "./NavSub";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";

let store;
let mockStore = configureMockStore([]);
let wrapper;

describe("NavSub.tsx", () => {
  jest
    .spyOn(React, "useState")
    .mockReturnValueOnce([{ active: true, message: "test message" }, {}])
    .mockReturnValueOnce([true, {}]);

  const storeData = {
    auth: {
      loginError: {
        active: true,
        message: "Test Error Message",
      },
    },
  };

  beforeEach(() => {
    store = mockStore(storeData);
    wrapper = shallow(
      <Provider store={store}>
        <NavSub />
      </Provider>
    );
  });

  test("Check that there are no links without state", () => {
    expect(wrapper.find(Anchor)).toHaveLength(0);
  });
});
