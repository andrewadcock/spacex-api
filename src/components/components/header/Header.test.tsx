import React from "react";
import NavSub from "../navSub/NavSub";
import { shallow } from "enzyme";
import Header from "./Header";
import NavMain from "../navMain/NavMain";
import Logo from "../logo/Logo";

describe("Header.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Header />);
  });

  test("renders navSub and navMain", () => {
    expect(wrapper.containsMatchingElement(<Logo />)).toEqual(true);
  });

  test("renders navSub and navMain", () => {
    expect(wrapper.containsMatchingElement(<NavSub />)).toEqual(true);
    expect(wrapper.containsMatchingElement(<NavMain />)).toEqual(true);
  });
});
