import React from "react";
import { mount } from "enzyme";
import { BrowserRouter } from "react-router-dom";
import RocketListItem from "./RocketListItem";

describe("RocketListItem.tsx", () => {
  let wrapper;

  const rocket = {
    height: {
      meters: 70,
      feet: 229.6,
    },
    flickr_images: [
      "https://farm1.staticflickr.com/929/28787338307_3453a11a77_b.jpg",
      "https://farm4.staticflickr.com/3955/32915197674_eee74d81bb_b.jpg",
      "https://farm1.staticflickr.com/293/32312415025_6841e30bf1_b.jpg",
      "https://farm1.staticflickr.com/623/23660653516_5b6cb301d1_b.jpg",
      "https://farm6.staticflickr.com/5518/31579784413_d853331601_b.jpg",
      "https://farm1.staticflickr.com/745/32394687645_a9c54a34ef_b.jpg",
    ],
    name: "Falcon 9",
    first_flight: "2010-06-04",
    id: "5e9d0d95eda69973a809d1ec",
  };

  beforeEach(() => {
    // Add the table to prevent errors in testing (can't have tr inside a div)
    wrapper = mount(
      <BrowserRouter>
        <table>
          <tbody>
            <RocketListItem rocket={rocket} />
          </tbody>
        </table>
      </BrowserRouter>
    );
  });

  test("Check number of table columns", () => {
    expect(wrapper.find("td")).toHaveLength(3);
  });

  test("Check that rocketListItem name renders", () => {
    expect(wrapper.find("td").at(1).text()).toEqual("Falcon 9");
  });

  test("Check that height renders", () => {
    expect(wrapper.find("td").at(2).text()).toEqual("229.6/70");
  });
});
