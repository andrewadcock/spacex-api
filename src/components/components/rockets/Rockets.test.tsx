import React from "react";
import { mount } from "enzyme";
import Rockets from "./Rockets";

describe("Rockets.tsx", () => {
  let wrapper;

  beforeEach(() => {
    // Add the table to prevent errors in testing (can't have tr inside a div)
    wrapper = mount(<Rockets />);
  });

  test("Check the title", () => {
    expect(wrapper.find("h3")).toHaveLength(1);
    expect(wrapper.find("h3").text()).toEqual("Rockets");
  });
});
