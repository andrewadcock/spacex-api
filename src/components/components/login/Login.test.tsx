import * as React from "react";
import { mount } from "enzyme";
import Login from "./Login";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";

let store;
let wrapper;
let mockStore = configureMockStore([]);

describe("Login.tsx", () => {
  const storeData = {
    auth: {
      loginError: {
        active: true,
        message: "Test Error Message",
      },
    },
  };

  beforeEach(() => {
    store = mockStore(storeData);
    wrapper = mount(
      <Provider store={store}>
        <Login />
      </Provider>
    );
  });

  test("Check that first name input exists", () => {
    expect(
      wrapper.find({ "data-testid": "first-name-input" }).exists()
    ).toBeTruthy();
  });
  //
  // test("Check that the shipListItem select exists", () => {
  //   expect(
  //     wrapper.find({ "data-testid": "shipListItem-select" }).exists()
  //   ).toBeTruthy();
  // });
  //
  // test("Check that the login submit button exists", () => {
  //   expect(
  //     wrapper.find({ "data-testid": "login-submit" }).exists()
  //   ).toBeTruthy();
  // });
});
