import React from "react";
import { mount } from "enzyme";
import ShipListItem from "./ShipListItem";
import { BrowserRouter } from "react-router-dom";

describe("ShipListItem.tsx", () => {
  let wrapper;

  const ship = {
    year_built: 2015,
    image: "https://i.imgur.com/28dCx6G.jpg",
    name: "Of Course I Still Love You",
    id: "5ea6ed30080df4000697c913",
    index: 9,
  };

  beforeEach(() => {
    // Add the table to prevent errors in testing (can't have tr inside a div)
    wrapper = mount(
      <BrowserRouter>
        <table>
          <tbody>
            <ShipListItem ship={ship} />
          </tbody>
        </table>
      </BrowserRouter>
    );
  });

  test("Check number of table columns", () => {
    expect(wrapper.find("td")).toHaveLength(3);
  });

  test("Check that shipListItem name renders", () => {
    expect(wrapper.find("td").at(1).text()).toEqual(
      "Of Course I Still Love You"
    );
  });

  test("Check that year built renders", () => {
    expect(wrapper.find("td").at(2).text()).toEqual("2015");
  });
});
