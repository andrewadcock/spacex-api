import React from "react";
import { shallow } from "enzyme";
import Anchor from "../../fields/anchor/Anchor";
import Profile from "./Profile";

let wrapper;

describe("NavSub.tsx", () => {
  beforeEach(() => {
    wrapper = shallow(<Profile />);
  });

  test("Check that there is a login link", () => {
    expect(wrapper.find(Anchor)).toHaveLength(1);
  });
});
