import React from "react";
import { mount } from "enzyme";
import Anchor from "./Anchor";
import IAnchor from "../../../interfaces/fields/anchor";
import { BrowserRouter } from "react-router-dom";

describe("Anchor.tsx", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(
      <BrowserRouter>
        <Anchor />
      </BrowserRouter>
    );
  });

  test("Check that one anchor is returned", () => {
    expect(wrapper.find("Link")).toHaveLength(1);
  });
});
