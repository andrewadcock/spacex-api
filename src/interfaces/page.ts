/**
 * Interface: Page
 */
import IAuth from "./auth";
import { RouteComponentProps } from "react-router-dom";
import { History, LocationState } from "history";

export default interface IPage {
  name: string;
  number: number;
  auth?: IAuth;
  userUpdate: (dataObject: object) => void;
  history?: History<LocationState>;
}
