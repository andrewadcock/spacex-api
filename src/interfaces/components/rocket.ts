/**
 * Interface: Rocket
 */
import IRocketItem from "./rocketItem";

export default interface IRocket {
  rocket?: IRocketItem;
}
