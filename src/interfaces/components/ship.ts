/**
 * Interface: ShipListItem
 */
import IShipItem from "./shipItem";

export default interface IShipListItem {
  ship: IShipItem;
}
