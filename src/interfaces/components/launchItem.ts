/**
 * Interface: LaunchItem
 */

export default interface ILaunchItem {
  launch: object;
  index: number;
}
