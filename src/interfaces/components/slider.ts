/**
 * Interface: Slider
 */

export default interface ISlider {
  slides: string[];
}
