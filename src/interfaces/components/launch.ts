/**
 * Interface: LaunchListItem
 */
import ILaunchItem from "./launchItem";

export default interface ILaunchListItem {
  launch: ILaunchItem;
}
