/**
 * Interface: NavSub
 */
import IAuth from "../auth";

export default interface INavSub {
  auth?: IAuth;
}
