/**
 * Interface: Login
 */

export default interface ILogin {
  userLogin?: (data: object) => void;
  error?: object;
}
