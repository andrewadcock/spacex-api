/**
 * Interface: ShipItem
 */

export default interface IShipItem {
  home_port?: string;
  id?: string;
  image: string;
  index: number;
  link?: string;
  name: string;
  type?: string;
  year_built: number;
}
