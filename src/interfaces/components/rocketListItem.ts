/**
 * Interface: RocketListItem
 */
import IRocketItem from "./rocketItem";

export default interface IRocketListItem {
  rocket: IRocketItem;
}
