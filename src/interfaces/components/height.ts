/**
 * Interface: Height
 */

export default interface IHeight {
  feet?: string;
  meters?: string;
}
