/**
 * Interface: Profile
 */
import ICurrentUser from "../store/currentUser";

export default interface IProfile {
  user?: ICurrentUser;
}
