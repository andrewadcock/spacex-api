/**
 * Interface: RocketItem
 */
import IHeight from "./height";

export default interface IRocketItem {
  id?: string;
  first_flight: string;
  flickr_images: string[];
  height: IHeight;
  index: number;
  name: string;
}
