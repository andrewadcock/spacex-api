/**
 * Interface: Alert
 */

export default interface IAlert {
  type?: string;
  message?: string;
}
