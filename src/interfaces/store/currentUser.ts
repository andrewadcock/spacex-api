/**
 * Interface: CurrentUser
 */
export default interface ICurrentUser {
  firstName?: string;
  favoriteRocket?: string;
}
