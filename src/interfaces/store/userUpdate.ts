/**
 * Interface: userUpdate
 */
import { RouteComponentProps } from "react-router-dom";

export default interface IUserUpdate extends RouteComponentProps {
  dataObject: object;
}
