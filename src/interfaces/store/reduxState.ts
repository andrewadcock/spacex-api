/**
 * Interface: ReduxState
 */
import IAuth from "../auth";
import ILoading from "./ILoading";

export default interface IReduxState {
  auth: IAuth;
  type?: string;
  loading: ILoading;
}
