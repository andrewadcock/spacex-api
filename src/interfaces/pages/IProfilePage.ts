/**
 * Interface: Profile Page
 */

import { RouteComponentProps } from "react-router-dom";
import IAuth from "../auth";

export default interface IProfilePage extends RouteComponentProps {
  auth?: IAuth;
}
