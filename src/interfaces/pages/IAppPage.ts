/**
 * Interface: App Page
 */

import { RouteComponentProps } from "react-router-dom";
import ILoading from "../store/ILoading";

export default interface IAppPage {
  loading?: ILoading;
  history?: any;
  location?: any;
  match?: any;
}
