/**
 * Interface: Anchor
 */

export default interface IAnchor {
  children: any;
  classes?: string;
  href: string;
  target?: string;
}
