/**
 * Interface: UserLoginError
 */

export default interface IUserLoginError {
  active?: boolean;
  message?: string;
}
