/**
 * Interface: UserLoginClearError
 */

export default interface IUserLoginClearError {
  type: object;
}
