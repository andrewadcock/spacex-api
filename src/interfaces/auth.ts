/**
 * Interface: Auth
 */
import ICurrentUser from "./store/currentUser";

export default interface IAuth {
  currentUser?: ICurrentUser;
  isLoggedIn?: boolean;
  loginError?: object;
}
