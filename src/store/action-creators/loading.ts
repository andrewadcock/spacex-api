/**
 * Action Creators - Loading
 */

import { ActionTypeLoading } from "../action-types";
import { Dispatch } from "redux";
import { ActionLoading } from "../actions/loading";

export const loadingActivate = () => {
  return (dispatch: Dispatch<ActionLoading>) => {
    dispatch({
      type: ActionTypeLoading.LOADING_ACTIVE,
    });
  };
};

export const loadingDeactivate = () => {
  return (dispatch: Dispatch<ActionLoading>) => {
    dispatch({
      type: ActionTypeLoading.LOADING_INACTIVE,
    });
  };
};
