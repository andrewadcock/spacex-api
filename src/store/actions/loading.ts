import { ActionTypeLoading } from "../action-types";

/**
 * Interface: Loading
 */

interface ILoadingActiveAction {
  type: ActionTypeLoading.LOADING_ACTIVE;
}

interface ILoadingInactiveAction {
  type: ActionTypeLoading.LOADING_INACTIVE;
}

export type ActionLoading = ILoadingActiveAction | ILoadingInactiveAction;
