import { ActionTypeUser, ActionTypeLoading } from "../action-types";
import { ActionLoading } from "./loading";
/**
 * Interface: Users
 *
 * What is an interface?
 *
 * An interface is used by TypeScript to ensure that all values being passed in are of the right type. This reduces
 * bugs and results in, ultimately, cleaner, more readable, less buggy code.
 */

interface IUserLoginAction {
  type: ActionTypeUser.USER_LOGIN;
  payload: object;
}

interface IUserLogoutAction {
  type: ActionTypeUser.USER_LOGOUT;
}

interface IUserUpdateAction {
  type: ActionTypeUser.USER_UPDATE;
  payload: object;
}

interface IUserLoginFailedAction {
  type: ActionTypeUser.USER_LOGIN_FAILURE;
  payload: string;
}

interface IUserLoginClearErrorAction {
  type: ActionTypeUser.USER_LOGIN_CLEAR_ERROR;
}

// Set up the action interface.
// The reducer action will check these in order to determine which to use for the specific use case.
export type ActionUser =
  | IUserLoginAction
  | IUserLogoutAction
  | IUserUpdateAction
  | IUserLoginClearErrorAction
  | IUserLoginFailedAction
  | ActionLoading;
