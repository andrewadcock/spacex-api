/**
 * TailwindCSS configuration
 *
 * This is fine but it could be better. How?
 * Given that the scope of this project is changing based on what cool things I can think of to do, I'm leaving all
 * the default colors. To keep the css size small, normally, I'd import tailwind colors and add just the ones I need.
 * For this, I'm just adding my own colors to the default object.
 *
 * @type {{plugins: *[], purge: string[], theme: {extend: {colors: {primary: {light: string, dark: string, DEFAULT: string}}}}, darkMode: boolean, variants: {extend: {}}}}
 */
module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: "#efeee0",
        },
        secondary: {
          DEFAULT: "#f0f",
        },
        tertiary: {
          DEFAULT: "#0ff",
        },
        background: {
          DEFAULT: "hsl(240, 2%, 12%)",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
