/**
 * Rules for craco
 *
 * Adds tailwindcss and autoprefixer support
 * @type {{style: {postcss: {plugins: ({}|postcss.Plugin<unknown>|{data?: {prefixes: {}|{}, browsers: *}, defaults?: *, info?: function(): *})[]}}}}
 */
module.exports = {
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },
};
