# SpaceX-Api

This is a basic app that ingests data from the SpaceX api. The purpose is to demonstrate knowledge of Axios, 
promises/await, state management (Redux), TypeScript, and clean code concepts. Much of this will be overkill for such
a small application. This is just a sample project to show React competencies. This is not meant to be taken seriously.

View this project live a [https://spacex.andrewadcock.com](spacex.andrewadcock.com)

## Usage
1. Clone the repo
   * SSH: `git clone git@gitlab.com:andrewadcock/spacex-api.git`
   * HTTPS: `git clone `https://gitlab.com/andrewadcock/spacex-api.git`
2. Move into the spacex-api dir
   * `cd spacex-api`
3. Start the local server
   * `npm start` or `yarn start`


## How to Review/Use
This application was built to showcase my React and JavaScript skill set. As you puruse the code
please note that several areas are built out more than this project really needs. If you make the 
assumption that this code base will continue to grow, things will make a bit more sense. 

## SpaceX API
The SpaceX API has several endpoints, however, they all follow the same basic query syntax. Instead of having 
many call functions that require a number of parameters (some more than 4) a single call with an object, 
admittedly on the larger side, seemed like an easier-to-manage solution. 

### API Options Object
List of available options
* endpoint (required): the specific endpoint (e.g. launches, ships, rockets)
* query (optional): the query filters
* options (required): additional query parameters
  * limit (optional): number of results to return
  * sort (optional): column and sort order object
  * offset (optional): query offset (e.g. start at record X)
  * select (optional): columns to return

Example
```
const options = {
      endpoint: "launches",
      query: {
        upcoming: {
          $eq: true,
        },
      },
      options: {
        limit: 10,
        sort: { name: "asc" },
        offset: queryOffset,
        select: {
          date_utc: 1,
          flight_number: 1,
          id: 1,
          name: 1,
          ships: 1,
        },
      },
```


## Redux
Redux is used throughout the site for authorization and user management. Thunks were added for async requests.
View `${ROOT}/src/store` for configuration details. 

## Misc
### Why is build in the repo?
Sometimes when making decisions like these we have to way outside costs. The hosting option that I've chosen
fails when attempting to build due to lake of memory. As such, the build files have been added until my hosting situation
is resolved.


## Author
Andrew is a senior software engineer with experience in React, Node, WordPress, Drupal, Laravel, Symfony, and 
Python/Django. With several years experience as a full-stack technical lead engineer, I am passionate about building
cohesive and productive teams while balancing client requirements and company-wide goals.